def accounting_format_of(amount):
    return format_or_none(amount, '.2f')


def int_or_none(daily_fees_input):
    return float_or_none(daily_fees_input)


def float_or_none(days_input):
    days = float(days_input) if days_input is not None else None
    return days


def format_or_none(o, f):
    return format(float(o), f) if o is not None else None
    

def some_string_or_none(str):
    return str if str != '' else None
