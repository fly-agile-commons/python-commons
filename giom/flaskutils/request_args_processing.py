import json
from functools import wraps

from flask import request


# Retrieve an args list wether its encoded as json or as form[data] or inline,
# If the results have a 'data' field, this will take over the result
# - if it's a  dict, will be returned as is
# - if it's a string that is json decodable, will be returned as decoded
# (@todo: in case of a json formatted string, it should instead be named 'json' instead of 'data'
# - otherwise returns it raw (string or bytes then)
def identify_request_args(req):
    maybe_data_args = get_data_arg_as_json_or_not(
        pick_json_or_form_or_querystring(req)
    )
    return maybe_data_args

def pick_json_or_form_or_querystring(req):
    return req.json if req.is_json else req.form if req.form else req.args


def get_data_arg_as_json_or_not(request_args_dict):
    if request_args_dict is None: return None
    return eventually_json_decode(eventually_zoom_on_data_field(request_args_dict))


def eventually_zoom_on_data_field(request_args_dict):
    return request_args_dict.get('data', request_args_dict)


def eventually_json_decode(args):
    if isinstance(args, str):
        try:
            args = json.loads(args)
        except ValueError as e:
            pass
    return args


def any_request_args(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        args_dict = identify_request_args(request)
        return f(args_dict, *args, **kwargs)

    return decorated


def extract_args(arg_extractor):
    def decorator(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            args_dict = identify_request_args(request)
            specific_args = arg_extractor(args_dict)
            return f(specific_args, *args, **kwargs)

        return decorated

    return decorator
