from abc import ABC

from flask import request
from werkzeug.security import check_password_hash

from giom.flaskutils.flask_authenticator import FlaskRequestAuthenticator, AuthenticationException, \
    ServerConfigException


class NoApiKeyError(AuthenticationException):
    def __init__(self, message="no API_KEY provided"):
        super().__init__(message)


class WrongApiKeyException(AuthenticationException):
    def __init__(self, api_key):
        super().__init__("api_key '%s' is not a match" % api_key)


class FlaskApiKeyHeaderValidator(FlaskRequestAuthenticator, ABC):
    API_KEY_NAME = "api_key"

    def __init__(self, app):
        super().__init__(app)
        self.API_KEY_NAME = "api_key"

    def check_request(self):
        if self.are_we_testing():
            self.app.logger.info("testing mode, ignoring authentication")
            return True

        return self.check_authentication()

    def check_authentication(self):
        api_key = self.get_api_key_from_request()
        if api_key is None:
            self.raise_no_api_key_provided()
        self.check_api_key(api_key)

    def raise_no_api_key_provided(self):
        raise NoApiKeyError()

    def get_api_key_from_request(self):
        return request.headers.get(self.API_KEY_NAME)

    def check_api_key(self, api_key):
        hashed_api_key = self.get_hashed_api_key()
        if not check_password_hash(hashed_api_key, api_key):
            self.raise_wrong_api_key_exception(api_key)

    def raise_no_api_key_provided(self):
        raise NoApiKeyError("no API_KEY found in request header")

    def raise_wrong_api_key_exception(self, api_key):
        raise WrongApiKeyException(api_key)

    def get_hashed_api_key(self):
        try:
            api_key = self.app.config["API_KEY"]
        except KeyError as e:
            self.raise_api_key_config_error()
        if api_key is None:
            self.raise_api_key_config_error()
        return api_key

    def raise_api_key_config_error(self):
        raise ServerConfigException('API_KEY not found in config')

    def set_hashed_api_key_of(self, key):
        self.app.config["API_KEY"] = key


class FlaskApiKeyParameterValidator(FlaskApiKeyHeaderValidator, ABC):

    def get_api_key_from_request(self):
        return request.args.get(self.API_KEY_NAME)

    def raise_no_api_key_provided(self):
        raise NoApiKeyError("no API_KEY found in request parameters")

