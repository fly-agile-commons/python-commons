import os
import random
import tempfile
from io import BytesIO, StringIO

import weasyprint
from flask_weasyprint import HTML, render_pdf, CSS
from pdf2image import convert_from_bytes

PAGE_SIZE = (1654, 2339)  # A4

DPI = 300


def url2image(relative_url: str) -> BytesIO:
    return HTML2image(HTML(relative_url))


def html2image(html_source: str) -> BytesIO:
    return HTML2image(HTML(StringIO(html_source)))


def HTML2image(html: weasyprint.HTML) -> BytesIO:
    css_string = """
    page {
        size: 1654px 2339px;
        margin:0;
    }
    """
    css = CSS(string=css_string)
    pdf = render_pdf(html, [css])
    debug_filename = os.path.join(tempfile.gettempdir(), f'url2image_intermediate_pdf-{random.randint(0, 999)}.png')
    with open(debug_filename, 'wb') as f:
        f.write(pdf.data)
    print(f'pdf generated saved at {debug_filename}')
    pdf_file = pdf.data
    images = convert_from_bytes(pdf_file, dpi=DPI)
    first_page = images[0]
    img_byte_arr = BytesIO()
    first_page.save(img_byte_arr, format='PNG', dpi=(DPI, DPI))
    # image_bytes = img_byte_arr.getvalue()
    img_byte_arr.seek(0)
    return img_byte_arr
