from abc import abstractmethod
from functools import wraps

from flask import Flask


class FlaskRequestAuthenticator:
    app: Flask

    def __init__(self, app):
        self.app = app

    @abstractmethod
    def check_request(self):
        pass

    def are_we_testing(self):
        return self.app.config['TESTING']

    @abstractmethod
    def handle_server_config_error(self, e):
        pass

    @abstractmethod
    def handle_wrong_authentication(self, e):
        pass

    def requires_auth(self, f):
        self.app.logger.debug("setting up the authentication")
        global c_app
        c_app = self.app

        @wraps(f)
        def decorated(*args, **kwargs):
            c_app.logger.info("checking the request")
            try:
                self.check_request()
            except AuthenticationException as e:
                return self.handle_wrong_authentication(e)
            except ServerConfigException as e:
                self.app.logger.error(e)
                return self.handle_server_config_error(e)

            return f(*args, **kwargs)

        return decorated


class AuthenticationException(Exception):
    pass


class ServerConfigException(Exception):
    pass
