from functools import wraps

import fitz
from flask import request
from flask_weasyprint import CSS, HTML, render_pdf


def pdf_convertible(stylesheets):
    def decorator(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            result = f(*args, **kwargs)
            response_format = request.headers.get('format')
            if response_format is not None and response_format is not None:
                return render_to_pdf(result, stylesheets)
            return result

        return decorated

    return decorator


def render_to_pdf(source, stylesheets):
    csss = [CSS(c) for c in stylesheets]
    return render_pdf(HTML(string=source), stylesheets=csss)


def is_font_present_in_pdf(pdf_data, font_name):
    # font_name = font_name.replace(' ', '-')
    total_fonts = extract_pdf_fonts(pdf_data)
    print(f'fonts found: {total_fonts}')
    return is_font_present(font_name, total_fonts)


def is_font_present(font_name, total_fonts):
    for font in total_fonts:
        if font_name in font:
            return True
    return False


def extract_pdf_fonts(pdf_data):
    doc = fitz.open(stream=pdf_data, filetype="pdf")
    total_fonts = []
    for page_num in range(len(doc)):
        page = doc[page_num]
        # Fetch the fonts used in the page
        fonts = page.get_fonts()
        for font in fonts:
            total_fonts.append(font[3][7::].replace('-', ' '))
    return total_fonts


def is_pdf_valid(pdf_data):
    try:
        return fitz.open(stream=pdf_data, filetype="pdf")
    except fitz.FileDataError as e:
        return False, e
