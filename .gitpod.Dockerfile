FROM gitpod/workspace-python:2023-03-24-22-45-37
USER root
RUN apt update -y && apt install -y weasyprint wkhtmltopdf
USER gitpod
COPY .python-version .python-version
RUN pyenv install $(cat .python-version)

