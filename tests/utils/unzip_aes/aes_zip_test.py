import os
from os.path import exists

import pytest
import pyzipper

from giom.utils.unzip_pdf \
    import decrypt_first_file_from_aes_zipfile, \
    list_files_in_aes_zipfile, \
    decrypt_file_from_aes_zipfile

class AesZipTestUtils(object):
    @pytest.fixture
    def an_aeszip_file(self):
        zipfile_name = os.path.join(os.path.dirname(__file__),
                                    'aes_zip_file_text.zip'
                                    )
        assert exists(zipfile_name), "sanity check: test path found"
        zipfile_encrypted = open(zipfile_name, 'rb')
        yield zipfile_encrypted
        zipfile_encrypted.close()

    @pytest.fixture
    def an_aeszip_file_pdf(self):
        zipfile_name = os.path.join(os.path.dirname(__file__),
                                    'aes_zip_file_pdf.zip'
                                    )
        assert exists(zipfile_name), "sanity check: test path found"
        zipfile_encrypted = open(zipfile_name, 'rb')
        yield zipfile_encrypted
        zipfile_encrypted.close()

    @pytest.fixture
    def content_file(self):
        with open(os.path.join(os.path.dirname(__file__), 'aTextFile.txt'), 'rb') as f:
            yield f
            f.close()

    @pytest.fixture
    def an_aeszip_stream(self, an_aeszip_file):
        yield an_aeszip_file.read()
        an_aeszip_file.close()

    @pytest.fixture
    def aeszip(self, an_aeszip_file):
        with pyzipper.AESZipFile(an_aeszip_file) as zf:
            zf.setpassword(b'thepassword')
            yield zf
            zf.close()


class TestAesUnzip(AesZipTestUtils):

    def test_can_read_namelist(self, aeszip):
        assert ['aTextFile.txt'] == aeszip.namelist()

    def test_can_open_encrypted_file(self, aeszip):
        # aeszip.testzip()
        aeszip.open('aTextFile.txt', 'r')

    def test_can_open_with_on_the_fly_password(self, aeszip):
        aeszip.open('aTextFile.txt', 'r', b'thepassword')

    def test_can_read_encrypted_file(self, aeszip):
        # Given
        # @Todo bring directly an AesZipFile as fixture
        the_file = aeszip.open('aTextFile.txt', 'r')

        original_file = open(os.path.join(os.path.dirname(__file__), 'aTextFile.txt'), 'rb')
        original_content = original_file.read()

        # Then
        # aeszip.testzip()
        content = the_file.read()
        assert original_content == content

    def test_cannot_open_with_wrong_password(self, aeszip):
        # aeszip.testzip()
        with pytest.raises(expected_exception=RuntimeError, match=r'(?i)bad password'):
            # When
            aeszip.open('aTextFile.txt', 'r', b'wrongpassword')

    def test_can_retrieve_filelist(self, aeszip):
        # Given
        # @Todo bring directly an AesZipFile as fixture
        # When
        filelist = aeszip.infolist()
        # Then
        assert len(filelist) == 1

    def test_can_decrypt_filenamelist_from_aes_zipfile(self, aeszip):
        # Given
        # @Todo bring directly an AesZipFile as fixture
        # When
        filelist = aeszip.infolist()
        # Then
        assert len(filelist) == 1
        assert filelist[0].filename == 'aTextFile.txt'



class TestDecryptAesZipfile(AesZipTestUtils):
    def test_can_decrypt_test_file_from_aes_zipfile(self, an_aeszip_file):
        assert an_aeszip_file.readable(), 'sanity check source is readable'
        filename, result = decrypt_first_file_from_aes_zipfile(an_aeszip_file, 'thepassword')
        assert result.startswith( b'Text file for Aes Zip encryption')
        assert filename == 'aTextFile.txt'

    def test_can_decrypt_first_file_on_aes_zipfile(self, an_aeszip_file_pdf):
        filename, result = decrypt_first_file_from_aes_zipfile(an_aeszip_file_pdf, 'thepassword')
        assert result.startswith(b'%PDF-1.3 \n%\xe2\xe3\xcf\xd3 \n1 0 obj \n<< \n/Type /Catalog \n/Pages 2 0')
        assert filename == 'PdfTest.pdf'

    def test_can_list_files_in_aes_zipfile(self, an_aeszip_file_pdf):
        filenames = list_files_in_aes_zipfile(an_aeszip_file_pdf, 'thepassword')
        assert filenames == ['PdfTest.pdf']

    def test_can_decrypt_random_files_in_aes_zipfile(self, an_aeszip_file_pdf):
        result = decrypt_file_from_aes_zipfile(an_aeszip_file_pdf, 'thepassword', 'PdfTest.pdf')
        assert result.startswith(b'%PDF-1.3 \n%\xe2\xe3\xcf\xd3 \n1 0 obj \n<< \n/Type /Catalog \n/Pages 2 0')
