import os
from io import BytesIO
from os.path import exists
from zipfile import ZipFile

import pytest as pytest

from giom.utils.unzip_pdf import extract_first_pdf_from_zip_bytes, extract_first_pdf_from_zipfile


class ZipTestUtils(object):
    pass


class TestUnzip(ZipTestUtils):

    def test_sanity_check_test_file_can_be_read(self):
        zipfile_name = os.path.join(os.path.dirname(__file__), 'file_to_decompress.zip')
        print("file %s" % zipfile_name)
        assert exists(zipfile_name), "sanity check: test path found"
        with open(zipfile_name, 'rb') as file:
            source = file.read()
            zipfile_encrypted = ZipFile(BytesIO(source))
            zipfile_encrypted.setpassword(bytes("thepassword", "utf-8"))
    #
            assert zipfile_encrypted.open("file_to_retrieve.txt"), "file can be read"

    def test_can_get_encrypted_file(self, zip_testfile):
        # Given
        # @Todo Could be encrpted there though
        # When
        text_file = extract_first_pdf_from_zipfile(zip_testfile, 'thepassword')
        # Then
        assert text_file.name == 'file_to_retrieve.txt', "filename matches"
        # assert text_file.read() == b'this is my encrypted file', "content matches"

    # def test_can_get_encrypted_stream(self, zipped_stream):
    #     # Given
    #     # @Todo Could be encrpted there though
    #     # When
    #     text_file_content = extract_first_pdf_from_zip_bytes(zipped_stream, 'thepassword')
    #     # Then
    #     assert text_file_content == b'this is my encrypted file', "content matches"

    @pytest.fixture
    def zip_testfile(self):
        zipfile_name = os.path.join(os.path.dirname(__file__), 'file_to_decompress.zip')
        assert exists(zipfile_name), "sanity check: test path found"
        with open(zipfile_name, 'rb') as zipfile_encrypted:
            yield zipfile_encrypted

    @pytest.fixture
    def zipped_stream(self, zip_testfile):
        yield zip_testfile.read()

    def test_cant_get_encrypted_file_with_bad_password(self, zipped_stream):
        # Given
        # @Todo Could be encrpted there though
        # Then
        with pytest.raises(expected_exception=RuntimeError, match=r'(?i)bad password'):
            # When
            text_file_content = extract_first_pdf_from_zip_bytes(zipped_stream, 'nope_password')
