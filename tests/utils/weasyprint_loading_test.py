
class TestWeasyPrintLoadSanityCheck():

    def test_should_load_weasyprint_without_a_fuss(self):
        try:
            import weasyprint
        except OSError as ose:
            assert False,    f'raised an OS exception "{ose}"'
