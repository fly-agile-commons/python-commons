import json

from flask import Response, jsonify
from pytest import fixture

from giom.flaskutils.request_args_processing import identify_request_args, any_request_args, pick_json_or_form_or_querystring, \
    eventually_zoom_on_data_field, eventually_json_decode
from tests.flaskutils.pdf_tests.flask_test_utils import create_test_app


class TestAnyRequestArgsDecorator:

    @fixture
    def appWithAnyRequestArgs(self):
        app = create_test_app(__name__, testing=True)

        @app.route('/')
        @any_request_args
        def any_request(args):
            return jsonify(args)

        app.add_url_rule('/', view_func=any_request)
        return app

    def test_pick_json_or_form_identifies_form_request(self, appWithAnyRequestArgs):
        with appWithAnyRequestArgs.test_request_context('/', data={'the input field': 'some content'}) as ctx:
            r = pick_json_or_form_or_querystring(ctx.request)
            assert dict(r) == {'the input field': 'some content'}

    def test_pick_json_or_form_identifies_json_request(self, appWithAnyRequestArgs):
        with appWithAnyRequestArgs.test_request_context('/', json={'the input field': 'some content'}) as ctx:
            r = pick_json_or_form_or_querystring(ctx.request)
            assert dict(r) == {'the input field': 'some content'}

    def test_eventually_select_nested_data_field_when_present(self):
        input = {
            'data':
                {'the input field': 'some content'},
            'not_a_nested_field': 'another content'
        }
        result = eventually_zoom_on_data_field(input)
        assert dict(result) == {'the input field': 'some content'}

    def test_eventually_json_decoded_if_json_formatted_str(self):
        input = '{"the input field": "some content"}'
        assert dict(eventually_json_decode(input)) == {'the input field': 'some content'}

    def test_eventually_json_decoded_if_str(self):
        input = "'the input field': 'some content'"
        assert isinstance(input, str), 'sanity check'
        assert eventually_json_decode(input) == "'the input field': 'some content'"

    def test_identify_request_args_handles_json(self, appWithAnyRequestArgs):
        with appWithAnyRequestArgs.test_request_context('/',
                                                        json={'the input field': 'some content'}) as ctx:
            args = identify_request_args(ctx.request)
            assert args == {'the input field': 'some content'}

    def test_identify_request_args_handles_query_string(self, appWithAnyRequestArgs):
        with appWithAnyRequestArgs.test_request_context('/',
                          query_string={'the input field': 'some content'}) as ctx:
            args = identify_request_args(ctx.request)
            assert dict(args) == {'the input field': 'some content'}

    def test_identify_request_args_handles_form(self, appWithAnyRequestArgs):
        with appWithAnyRequestArgs.test_request_context('/',
                                                        data={'the input field': 'some content'}) as ctx:
            args = identify_request_args(ctx.request)
            assert dict(args) == {'the input field': 'some content'}

    def test_identify_request_args_picks_json_over_query_string(self, appWithAnyRequestArgs):
        with appWithAnyRequestArgs.test_request_context('/',
                                                        json={'the input field': 'some content'}, query_string={'not the field':'something'}) as ctx:
            args = identify_request_args(ctx.request)
            assert args == {'the input field': 'some content'}

    def test_identify_request_args_picks_form_over_query_string(self, appWithAnyRequestArgs):
        with appWithAnyRequestArgs.test_request_context('/',
                                                        data={'the input field': 'some content'},
                                                        query_string={'not the field': 'something'}) as ctx:
            args = identify_request_args(ctx.request)
            assert dict(args) == {'the input field': 'some content'}

    def test_identify_data_args_returns_empty_if_none_given(self, appWithAnyRequestArgs):
        with appWithAnyRequestArgs.test_request_context('/') as ctx:
            args = identify_request_args(ctx.request)
            assert dict(args) == {}

    def test_can_handle_form_parameters(self, appWithAnyRequestArgs):
        r: Response = (appWithAnyRequestArgs.test_client().get('/', data={'the input field': 'some content'}))
        assert r.status_code == 200
        assert r.json == {'the input field': 'some content'}

    def test_answer_handle_nested_json_encoded_data(self, appWithAnyRequestArgs):
        r = appWithAnyRequestArgs.test_client().get('/', json={'data': json.dumps({'the input field': 'some content'})})
        assert r.status_code == 200

    def test_answer_handle_plain_query_string_data(self, appWithAnyRequestArgs):
        r = appWithAnyRequestArgs.test_client().get('/?the_input_field=some_content')
        assert r.status_code == 200
        assert r.json == {'the_input_field': 'some_content'}
