import pytest
from flask import Flask, url_for
import os


def create_test_app(name, testing=True):
    app = Flask(name, static_folder=os.path.join(os.path.dirname(__file__),
                                                 'test_data'))  # mentioning static_folder for readibility
    app.testing = testing
    app.config['SERVER_NAME'] = 'localhost'
    return app


class FlaskContextTestUtils:
    @pytest.fixture
    def app(self):
        return create_test_app(__name__, testing=True)
