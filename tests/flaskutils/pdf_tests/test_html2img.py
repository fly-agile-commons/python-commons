import os
import random

from PIL import Image
import tempfile
import pytest
from pytest import fixture, skip

from giom.flaskutils.html2img import url2image
from tests.flaskutils.pdf_tests.flask_test_utils import FlaskContextTestUtils, create_test_app

SIMPLE_HTML_SOURCE_PATH = os.path.join(os.path.dirname(__file__), 'test_data/simple_sourcefile.html')
SIMPLE_PDF_RESULT_PATH = os.path.join(os.path.dirname(__file__), 'test_data/simple_sourcefile_expected_result.pdf')
SIMPLE_IMG_RESULT_PATH = os.path.join(os.path.dirname(__file__), 'test_data/simple_sourcefile_expected_result.png')
COMPLEX_HTML_SOURCE_PATH = os.path.join(os.path.dirname(__file__), 'test_data/complex_sourcefile.html')
COMPLEX_PDF_RESULT_PATH = os.path.join(os.path.dirname(__file__), 'test_data/complex_sourcefile_expected_result.pdf')
COMPLEX_IMG_RESULT_PATH = os.path.join(os.path.dirname(__file__), 'test_data/complex_sourcefile_expected_result.jpg')


class Html2ImgTestUtils:

    def simple_html_source(self):
        with open(SIMPLE_HTML_SOURCE_PATH, mode='r') as source:
            return source.read()

    @fixture()
    def simple_img_expected(self) -> Image:
        return Image.open(open(SIMPLE_IMG_RESULT_PATH, mode='rb'))
        # with open(SIMPLE_IMG_RESULT_PATH, mode='rb') as expected_output_file:
        #     return Image.open(io.BytesIO(expected_output_file.read())

    def complex_html_source(self):
        with open(COMPLEX_HTML_SOURCE_PATH, mode='r') as source:
            return source.read()

    @fixture
    def complex_expected_img(self):
        return Image.open(open(COMPLEX_IMG_RESULT_PATH, mode='rb'))

    @fixture
    def app_serving_html(self):
        a = create_test_app(__name__, testing=True)
        with a.app_context():
            a.add_url_rule('/simple', view_func=self.simple_html_source)
            a.add_url_rule('/complex', view_func=self.complex_html_source)
        return a

    def save_img(self, img: Image, file_prefix):
        result_filename = os.path.join(tempfile.gettempdir(), f'{file_prefix}{random.randint(0, 999)}.png')
        img.save(result_filename)
        return result_filename


class TestHtml2Img(Html2ImgTestUtils):

    def assert_images_match(self, img1: Image, img2: Image, threshold=0.1):
        """
        Compare two images with tolerance for minor differences
        threshold: percentage of pixels that can be different (0.1 = 10%)
        """
        # Convert binary data to PIL Images
        self.assert_same_size(img1, img2)

        # Convert to same format if needed
        self.assert_almost_identical(img1, img2, threshold)

    def assert_almost_identical(self, img1, img2, threshold):
        img1 = img1.convert('RGB')
        img2 = img2.convert('RGB')
        # Compare pixels
        pairs = zip(img1.getdata(), img2.getdata())
        diff = sum(1 for p1, p2 in pairs if p1 != p2)
        total_pixels = img1.size[0] * img1.size[1]
        percentage_diff = diff / total_pixels
        assert percentage_diff <= threshold, f'image difference is higher than {threshold} ({percentage_diff}%)'

    def assert_same_size(self, img1: Image, img2: Image, msg="not the same size"):
        # Ensure same size
        assert img1.size == img2.size, msg

    def assert_request_converted_to_img(self, app_serving_html, source_url, expected_img: Image):
        with app_serving_html.app_context():
            with app_serving_html.test_client() as c:
                response = c.get(source_url)
                img = Image.open(url2image(source_url))

            print(f"Generated image size: {img.size if img else 'None'}")
            print(f"Expected image size: {expected_img.size if expected_img else 'None'}")

            log_name_prefix = source_url.lstrip('/')
            result_filename = self.save_img(img, f'{log_name_prefix}-img-result')

            try:
                self.assert_images_match(img, expected_img)
            except AssertionError as e:
                expected_filename = self.save_img(expected_img, f'{result_filename}-expected')
                print(
                    f'see generated file://{result_filename} and expected file://{expected_filename}'
                )
                raise

    def test_convert_simple_html_to_image(self, app_serving_html, simple_img_expected):
        self.assert_request_converted_to_img(app_serving_html, '/simple', simple_img_expected)

    def test_convert_complex_online_html_to_image(self, app_serving_html, complex_expected_img):
        pytest.skip('someday')
        self.assert_request_converted_to_img(app_serving_html, '/complex', complex_expected_img)
