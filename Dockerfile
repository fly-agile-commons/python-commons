FROM python:3.12-slim-bullseye AS builder

ARG WKHTMLTOPDF_VERSION=0.12.6

RUN sed -i'.bak' 's/$/ contrib/' /etc/apt/sources.list && apt-get update

RUN apt-get update && apt-get install -y \
        fontconfig \
        fonts-dejavu-core \
        poppler-utils=20.* \
        libpango-1.0-0 \
        libpangocairo-1.0-0 \
        ttf-mscorefonts-installer \
        xorg libxrender-dev &&\
        apt-get clean && \
        rm -rf /var/lib/apt/lists/* && \
        fc-cache -f

WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt

FROM builder AS test
COPY . .
CMD ["pytest", "-v", "--cov=giom"]

FROM builder AS development
RUN pip install pytest-watch pytest-xdist
WORKDIR /app
CMD ["ptw", "--", "-v", "--cov=giom"]