from setuptools import setup

setup(
    name='giomutils',
    version='1.7.1',
    description='utils stuff for giom infra',
    url='https://gitlab.com:fly-agile-commons/python-commons.git',
    author='Giom Duquesnay',
    author_email='guillaume.duquesnay@gmail.com',
    license='BSD 2-clause',
    packages=['giom.utils', 'giom.flaskutils'],
    install_requires=[
        'Flask>=3.1.0',
        'flask-weasyprint~=1.1.0',
        'weasyprint~=63.1',
        'pyzipper>=0.3.6',
        'pdf2image~=1.17.0',
        'pdfkit~=1.0.0',
        'pillow~=11.1.0',
        'PyMuPDF~=1.24.14'
    ],

    classifiers=[
        'License :: OSI Approved :: BSD License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.9',
    ],
)

# @todo split into separate independant packages, to be imported one by one?